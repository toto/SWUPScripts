#!/bin/bash
#SBATCH --job-name=OSFeatures
#SBATCH --mem=4Gb
#SBATCH -n 10
#SBATCH -t 12:00:00
python extractOSFeatures.py --folder $1 --outputfile $2$(basename $1)OpenSmile.csv --configfile $3 --dataset $4
