#!/bin/bash

INFILE="$1"
CLIP_LEN="$2"
OVERLAP="$3"
OUTDIR="$4"
echo Splitting $INFILE in $CLIP_LEN second clips  with $OVERLAP overlap. Output into $OUTDIR

# Extract clip length from wav file
L=$(sox "$INFILE" -n stat 2>&1 | sed -n 's#^Length (seconds):[^0-9]*\([0-9.]*\)$#\1#p')
echo Length is $L
# Num Clips  x is x = L/n (rounded)
NUM_CLIPS=$(echo "($L/$CLIP_LEN+$OVERLAP)/(1-$OVERLAP)" | bc -l)
MORETHANONE=$(echo $NUM_CLIPS'>'1 | bc -l)
echo Number of CLips is:  $NUM_CLIPS
echo More than one is: $MORETHANONE
if [ $MORETHANONE = 0 ]; then
        echo cp "$INFILE" $OUTDIR/$(basename $INFILE)_0.wav
        cp "$INFILE" $OUTDIR/$(basename $INFILE)_1.wav
else
# Alternative bashism [ $x =0 ] && cp $1 $4/${1}_0.wav 
	for i in $(seq 1 "$NUM_CLIPS");
	do
		if [ $i = 1 ]; then
			#CLIP START
    		START=0
    		#CLIP END
    		END="$CLIP_LEN"      
   	 else
		#CLIP START
   	 	START=$(echo $END-$OVERLAP*"$CLIP_LEN" | bc -l)
    		#CLIP END
    		END=$(echo $START+"$CLIP_LEN" | bc -l)    
    	fi
		echo sox $INFILE $OUTDIR/$(basename $INFILE)_$(echo $i).wav trim $START $END
		sox "$INFILE" $OUTDIR/$(basename $INFILE)_$(echo $i).wav trim $START $END

	done    
fi
