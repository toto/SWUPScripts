import csv
import os, sys
import getOpenSmileFeatures as osf
import argparse


OSPATH = "/work/shared/mhealthmqp/opensmile-2.3.0"
OSEXEC = "/SMILExtract"
OSCONF = "/config"
configfile = "avec2013.conf"
dataset = "DAIC"
isHeader = True
parser = argparse.ArgumentParser()

parser.add_argument("--folder", help="Input Folder")
parser.add_argument("--dataset", help="Dataset default: DAIC")
parser.add_argument("--outputfile", help="Outputfile")
parser.add_argument("--configfile", help="Config filename, default: avec2013.conf")
parser.add_argument("--isHeader", type=bool, help="Should the output file contain a header. Default=True")
args = parser.parse_args()


if args.__dict__["folder"]  is not None:
    folder = args.__dict__["folder"]
if args.__dict__["outputfile"]  is not None:
    outputfile = args.__dict__["outputfile"]
if args.__dict__["configfile"]  is not None:
    configfile = args.__dict__["configfile"]
if args.__dict__["dataset"]  is not None:
    dataset = args.__dict__["dataset"]
if args.__dict__["isHeader"]  is not None:
    isHeader = args.__dict__["isHeader"]

f = open(outputfile,"w+")
for filename in os.listdir(folder):	
        f.write(osf.getFeatures(folder+filename,configfile,OSPATH,OSEXEC,OSCONF,isHeader,dataset)+"\n")
        isHeader = False
f.close()
