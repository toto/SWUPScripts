#!/bin/bash
#SBATCH --job-name=Jupyter_Notebook
#SBATCH --mem=8G
#SBATCH --gres=gpu:1

date;hostname;pwd

################################
# How to use this script?
# 1) On the cluster head node, batch submit this script: sbatch tensorboard_begin.sh
# 2) Open the tensorboard.log file and copy the line "SSH tunnel command", starting from "ssh - NL ..."
# 3) On your local computer (laptop) terminal, paste the above command to build the ssh tunnel.
# 4) On your local computer, open a browser and visit: http://localhost:{port}
################################

export XDG_RUNTIME_DIR=""

port=$(shuf -i 6000-7000 -n 1)
echo -e "\nStarting Jupyter on port ${port} on the $(hostname) server."
echo -e "\nSSH tunnel command: ssh -NL 6600:$(hostname):${port} ace.wpi.edu"
echo -e "\nLocal URI: http://localhost:6600"

jupyter-notebook --notebook-dir=./ --no-browser --port=${port} --ip='*'
