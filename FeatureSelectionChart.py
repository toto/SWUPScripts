#!/usr/bin/env python
# coding: utf-8

# In[17]:


# Feature Importance
from sklearn import datasets
from sklearn import metrics
from sklearn.ensemble import ExtraTreesClassifier
import argparse
import pandas as pd # Pandas is used to import the CSV file
import numpy as np
import random 
import analyzerTools as at
import operator
import json
import argparse

importancethreshold = 0.2
cached = True
dataStart = 5
dataEnd = -1
filename = "./FeatureFiles/DAIC/Labeled_NoMissing_originalOpenSmile.csv"   
random.seed(123)
cutoff = 10
targetData = -1 #last feature used as class variable. 
targetDataCount = 1
resampleType = "up" #up, down
svckernel = "linear" # ‘linear’, ‘poly’, ‘rbf’, ‘sigmoid’, ‘precomputed’ or a callable. If none is given: ‘rbf’
missingValues = -999 #Remove Instances: Remove, 0, -100, -1
modelTypes = ["RF","kNN"]
numNeighbors = 6 # k for kNN (number of neighbours)
gridParams = "None"
optimizeFor = "f1"
identifierList = '{"ParticipantID": 0}'
importanceRange = '{"Start": 0.1, "End": 4,"Increment":0.1}'
identifierList = json.loads(identifierList)
uniqueIdentifier = list(identifierList.keys())[0]



parser = argparse.ArgumentParser()

parser.add_argument("--importanceRange", type=float, help='Importance threshold as average multiplier range: default {"Start": 0.1, "End": 4,"Increment":0.1}')
parser.add_argument("--filename", help="Input File")
parser.add_argument("--foldcache", help="Fold Cache File")
parser.add_argument("--outputfolder", help="Output Folder")

args = parser.parse_args()

if args.__dict__["filename"]  is not None:
    filename = args.__dict__["filename"] 
if args.__dict__["foldcache"]  is not None:
    foldcache = args.__dict__["foldcache"]     
if args.__dict__["outputfolder"]  is not None:
    outputfolder = args.__dict__["outputfolder"]     
if args.__dict__["importanceRange"]  is not None:
    importanceRange = args.__dict__["importanceRange"]    
importanceRange = json.loads(importanceRange)    
  
data = pd.read_csv(filename)

with open(foldcache) as json_file:
    participantIdFolds = json.load(json_file) 

print(importanceRange)
print(modelTypes)
    
for modelType in modelTypes:
    for importancethreshold in np.arange(importanceRange["Start"],importanceRange["End"],importanceRange["Increment"]):
        print("Generating Features for impartance: " + str(importancethreshold) + " model: " + modelType + " input " + filename + "output: " +  outputfolder)
        dataStart = 5
        dataEnd = -1
        data = pd.read_csv(filename)
        featureSubset = data[data.columns[dataStart:dataEnd]] #Skip PHQ-9 Responses
        target = data[data.columns[targetData]]
        target = np.where(target > cutoff, 1, 0)
        dataNames = data.columns.values 
        featureNames = featureSubset.columns.values 

        #print(np.mean(target))
        # fit an Extra Trees model to the data
        model = ExtraTreesClassifier()
        model.fit(featureSubset, target)
        # display the relative importance of each attribute
        importances = model.feature_importances_ #array with importances of each feature
        idx = np.arange(0, featureSubset.shape[1]) #create an index array, with the number of features
        print(np.mean(importances)*importancethreshold)
        features_to_keep_indeces = idx[importances > np.mean(importances)*importancethreshold] #only keep features whose importance is greater than the mean importance
        #should be about an array of size 3 (about)
        features_to_keep_names = list(dataNames[range(0,dataStart)]) + list(featureNames[features_to_keep_indeces]) + list([dataNames[-1]])
        data = data[features_to_keep_names]
        resultsfile = outputfolder + "results_"+modelType+"_FeatureSelected_Labeled_NoMissing_"+str(importancethreshold)+"_"+str(data.shape[1])+".csv"

        # In[18]:



        if missingValues == '-999':
            data = data.dropna()
        else:
            data = data.replace(np.nan, missingValues, regex=True) # Replace all missing values with missingValues as defined


        #Add Binary Class
        binary_classes = np.where(data[data.columns[targetData]] > cutoff, 1, 0)
        data["binary_class"] = binary_classes

        #Adujst for the addition of binary label
        upToLabels = (targetDataCount*-1)-1
        binaryLabel = targetData
        numericLabel = targetData - 1

        if resampleType != "None":
            data = at.resampleDataset(data,resampleType,binaryLabel)



        from sklearn import metrics
        from sklearn import svm
        #from statistics import mean 
        from sklearn.ensemble import RandomForestClassifier
        from sklearn.neighbors import KNeighborsClassifier
        from sklearn.multiclass import OneVsRestClassifier
        from sklearn.gaussian_process import GaussianProcessClassifier
        from sklearn.gaussian_process import GaussianProcessRegressor
        from sklearn.gaussian_process.kernels import RBF
        from sklearn.neural_network import MLPClassifier
        from sklearn.ensemble import RandomForestRegressor
        from sklearn.neighbors import KNeighborsRegressor
        from sklearn.svm import LinearSVC
        from sklearn.svm import LinearSVR

        #from sklearn.svm import SVR
        #Cross Validation will happen with predefined folds for replicability
        #See script for generating folds
        #CV is implemented from scratch instead of using the scikit
        #Default CV to have more control of the underlying mechanism, as this might
        #be required by metalgorithms such as SCB (Toto et al. 2018 ECML-PKDD)
        master_y_pred = np.array(0)
        master_y_score = np.array(0)
        master_y_true = np.array(0)
        firstFold = True

        for fold in participantIdFolds:
            trainids = participantIdFolds[fold]["TRAIN"]
            testids  = participantIdFolds[fold]["TEST"]
            trainset = data[data[uniqueIdentifier].isin(trainids)]
            testset  = data[data[uniqueIdentifier].isin(testids)]
            #SVC, RF, kNN
            if modelType == "SVM":
                clf = svm.SVC(kernel=svckernel)
                regressor = svm.LinearSVR()
            elif modelType == "RF":
                n_estimators=30
                clf = RandomForestClassifier(n_estimators=n_estimators, max_depth=10)
                regressor = RandomForestRegressor(n_estimators=n_estimators)
            elif modelType == "kNN":
                clf = KNeighborsClassifier(n_neighbors=numNeighbors)
                regressor = KNeighborsRegressor(n_neighbors=numNeighbors)
            elif modelType == "GP":
                kernel = 1.0 * RBF(1.0)
                clf = GaussianProcessClassifier(kernel=kernel, random_state=0)
                regressor = GaussianProcessRegressor(kernel=kernel, n_restarts_optimizer=10, alpha=0.1, normalize_y=True)

            print(fold)
            #Classification Model with Binary Labels and Predictions
            clf.fit(trainset[trainset.columns[dataStart:upToLabels]], trainset[trainset.columns[binaryLabel]])
            y_pred = clf.predict(testset[testset.columns[dataStart:upToLabels]])
            y_score = clf.predict_proba(testset[testset.columns[dataStart:upToLabels]])
            #Ground Truth for Classification
            y_true = np.array(testset[testset.columns[binaryLabel]])

            #Regression Model and Predictions
            regressor.fit(trainset[trainset.columns[dataStart:upToLabels]], trainset[trainset.columns[numericLabel]])
            y_pred_reg = regressor.predict(testset[testset.columns[dataStart:upToLabels]]) 

            #Ground Truth for Regression
            y_true_numeric = np.array(testset[testset.columns[numericLabel]])
            #Combine Fold Results
            if firstFold:
                master_y_pred = y_pred
                master_y_score = y_score
                master_y_true = y_true 
                master_y_pred_reg = y_pred_reg
                master_y_true_numeric = y_true_numeric 
                master_y_testids = testset[testset.columns[:dataStart]]
            else:
                master_y_pred = np.concatenate((master_y_pred,y_pred),axis=None)
                master_y_score = np.concatenate((master_y_score,y_score),axis=0)
                master_y_pred_reg = np.concatenate((master_y_pred_reg,y_pred_reg),axis=None)
                master_y_true = np.concatenate((master_y_true,y_true),axis=None)
                master_y_true_numeric = np.concatenate((master_y_true_numeric,y_true_numeric),axis=None)
                master_y_testids = np.concatenate((master_y_testids,testset[testset.columns[:dataStart]]),axis=0)
            firstFold = False
            print(fold)
        isId = True
        for key, value in identifierList.items():
            if isId:
                finalResults = pd.DataFrame({key:master_y_testids[:,value].tolist()})
                isId = False
            else:
                finalResults[key] = master_y_testids[:,value].tolist()
        print("CHECK 1")
        finalResults['binaryPrediction'] = master_y_pred.tolist()
        finalResults['probabilityOfZero'] = master_y_score[:,0].tolist()
        finalResults['probabilityOfOne'] = master_y_score[:,1].tolist()
        finalResults['regressionPrediction'] = master_y_pred_reg.tolist()
        finalResults['binaryTruth'] = master_y_true.tolist()
        finalResults['regressionTruth'] = master_y_true_numeric.tolist()
        finalResults.to_csv(resultsfile, sep=',')   
        print("CHECK 2")



        # In[ ]:




