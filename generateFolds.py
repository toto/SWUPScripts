# Fold Generation
from sklearn.model_selection import KFold
import argparse
import pandas as pd # Pandas is used to import the CSV file
import numpy as np
import json
import os

forceOverwrite = False
foldcache = "foldchache.json"
uniqueIdentifier = "ParticipantID"
numFolds = 5
parser = argparse.ArgumentParser()
randomState = 42
parser.add_argument("--uniqueIdentifier", help="Unique Identifier Index. Default = ParticipantID")
parser.add_argument("--numFolds", type=int, help="Number of Folds. Default = 5")
parser.add_argument("--randomState", type=int, help="Random State Initializer. Default = Check Hitchikers Guide to The Galaxy for a special int ;)")

parser.add_argument("--filename", help="Input filename")
parser.add_argument("--foldcache", help="Fold Cache File. Default=foldchache.json")
parser.add_argument("--forceOverwrite", type=bool, help="Overwrite Fold Cache File. This may invalidate your experiments. Default=False")

args = parser.parse_args()

if args.__dict__["uniqueIdentifier"]  is not None:
    uniqueIdentifier = args.__dict__["uniqueIdentifier"]
if args.__dict__["filename"]  is not None:
    filename = args.__dict__["filename"]
if args.__dict__["foldcache"]  is not None:
    foldcache = args.__dict__["foldcache"]
if args.__dict__["forceOverwrite"]  is not None:
    forceOverwrite = args.__dict__["forceOverwrite"]
if args.__dict__["numFolds"]  is not None:
    numFolds = args.__dict__["numFolds"]    
if args.__dict__["randomState"]  is not None:
    randomState = args.__dict__["randomState"]  
    
data = pd.read_csv(filename)
identifierData = data[uniqueIdentifier]
kf = KFold(n_splits=numFolds,random_state=42, shuffle=True)

fold = 1
folds = {}

for train_index, test_index in kf.split(identifierData):
    #print("TRAIN:", train_index, "TEST:", test_index)
    folds[fold] = {}
    folds[fold]["TRAIN"] = identifierData.loc[train_index].tolist()
    folds[fold]["TEST"] = identifierData.loc[test_index].tolist()
    fold = fold + 1
if not forceOverwrite and os.path.isfile(foldcache):
        print ("Cached Folds Exist. Please Use a different Fold Cache File. Overwriting can invalidate your experiments.\n If you wish to continue use the --forceOverwrite option.")    
else:
    with open(foldcache, 'w', encoding='utf-8') as json_cache_file:
        json.dump(folds, json_cache_file, ensure_ascii=False, indent=4)
