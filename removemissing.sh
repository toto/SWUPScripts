#!/bin/bash
for filename in $*; do
	echo From "$filename" to "NoMissing_$filename"
		awk -F, '$20!=""' "$filename" > "NoMissing_$filename"
done
