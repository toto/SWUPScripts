INFILE="$1"
CLIP_LEN="$2" #Do nothing, calculate this value for this verison
OVERLAP="$3"
OUTDIR="$4"
NUM_CLIPS="$5"
echo Splitting $INFILE in $NUM_CLIPS clips  with $OVERLAP overlap. Output into $OUTDIR

# Extract clip length from wav file
L=$(sox "$INFILE" -n stat 2>&1 | sed -n 's#^Length (seconds):[^0-9]*\([0-9.]*\)$#\1#p')
echo Length is $L
# Clip length x is x = L/(NUM_CLIPS-(NUM_CLIPS-1)OVERLAP)
x=$(echo $NUM_CLIPS-1 | bc -l)
x=$(echo $x*$OVERLAP | bc -l)
x=$(echo $NUM_CLIPS-$x | bc -l)
x=$(echo $L/$x | bc -l)
CLIP_LEN=$x

echo Number of CLips is:  $NUM_CLIPS
echo Clip Length is:  $CLIP_LEN
for i in $(seq 1 "$NUM_CLIPS");
    do
            if [ $i = 1 ]; then
                    #CLIP START
            START=0
            #CLIP END
            END="$CLIP_LEN"
     else
            #CLIP START
            START=$(echo $END-$OVERLAP*"$CLIP_LEN" | bc -l)
            #CLIP END
            END=$(echo $START+"$CLIP_LEN" | bc -l)
    fi
            echo sox $INFILE $OUTDIR/$(basename $INFILE)_$(echo $i).wav trim $START $END
            sox "$INFILE" $OUTDIR/$(basename $INFILE)_$(echo $i).wav trim $START $END

done
