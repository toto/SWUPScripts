#!/bin/bash
sbatch splitjob.sh './MoodableEMU/original/*.wav' ./split.sh 2 0 ./MoodableEMU/2secOver0
sbatch splitjob.sh './MoodableEMU/original/*.wav' ./split.sh 2 0.25 ./MoodableEMU/2secOver0.25
sbatch splitjob.sh './MoodableEMU/original/*.wav' ./split.sh 2 0.5 ./MoodableEMU/2secOver0.5
sbatch splitjob.sh './MoodableEMU/original/*.wav' ./split.sh 2 0.75 ./MoodableEMU/2secOver0.75
sbatch splitjob.sh './MoodableEMU/original/*.wav' ./split.sh 3 0 ./MoodableEMU/3secOver0
sbatch splitjob.sh './MoodableEMU/original/*.wav' ./split.sh 3 0.25 ./MoodableEMU/3secOver0.25
sbatch splitjob.sh './MoodableEMU/original/*.wav' ./split.sh 3 0.5 ./MoodableEMU/3secOver0.5
sbatch splitjob.sh './MoodableEMU/original/*.wav' ./split.sh 3 0.75 ./MoodableEMU/3secOver0.75
