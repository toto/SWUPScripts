from sklearn.utils import resample
import collections
import pandas as pd # Pandas is used to import the CSV file

def resampleDataset(dataset,resampleType,targetData):
    #Identify majority and miniority class for upsample/downsample proceedures
    targetClassCount = collections.Counter(dataset[dataset.columns[targetData]])
    majorityKey = max(targetClassCount, key=targetClassCount.get)
    majorityCount = targetClassCount[majorityKey]
    minorityKey = min(targetClassCount,  key=targetClassCount.get)
    minorityCount = targetClassCount[minorityKey]    
    #Separate minority and majority classes
    dataset_majority = dataset[dataset[dataset.columns[len(dataset.columns)-1]] == majorityKey]
    dataset_minority = dataset[dataset[dataset.columns[len(dataset.columns)-1]] == minorityKey]

    if resampleType == "up":
        # Upsample minority class with replacement
        dataset_minority_upsampled = resample(dataset_minority,replace=True,n_samples=majorityCount) 

        # Combine majority class with upsampled minority class
        dataset_upsampled = pd.concat([dataset_majority, dataset_minority_upsampled])
        dataset_upsampled = dataset_upsampled.sample(frac=1).reset_index(drop=True)
        dataset = dataset_upsampled
    elif resampleType == "down":
        # Downsample majority class without replacement
        dataset_majority_downsampled = resample(dataset_majority,replace=False,n_samples=minorityCount) 
        # Combine majority class with upsampled minority class
        dataset_downsampled = pd.concat([dataset_majority_downsampled, dataset_minority])
        dataset_downsampled = dataset_downsampled.sample(frac=1).reset_index(drop=True)    
        dataset = dataset_downsampled
    return dataset