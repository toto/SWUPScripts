#!/bin/bash
sbatch splitjob.sh './EMU/original/*.wav' ./split.sh 2 0 ./EMU/2secOver0
sbatch splitjob.sh './EMU/original/*.wav' ./split.sh 2 0.25 ./EMU/2secOver0.25
sbatch splitjob.sh './EMU/original/*.wav' ./split.sh 2 0.5 ./EMU/2secOver0.5
sbatch splitjob.sh './EMU/original/*.wav' ./split.sh 2 0.75 ./EMU/2secOver0.75
sbatch splitjob.sh './EMU/original/*.wav' ./split.sh 3 0 ./EMU/3secOver0
sbatch splitjob.sh './EMU/original/*.wav' ./split.sh 3 0.25 ./EMU/3secOver0.25
sbatch splitjob.sh './EMU/original/*.wav' ./split.sh 3 0.5 ./EMU/3secOver0.5
sbatch splitjob.sh './EMU/original/*.wav' ./split.sh 3 0.75 ./EMU/3secOver0.75
