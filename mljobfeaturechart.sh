#!/bin/bash
#SBATCH --job-name=MLJob
#SBATCH --mem=9Gb
#SBATCH -n 8
#SBATCH -t 12:00:00

python FeatureSelectionChart.py --importancethreshold $1
