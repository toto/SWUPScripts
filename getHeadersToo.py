import os

def getFeatures(inputfile,configfile,OSPATH,OSEXEC,OSCONF):
	filename, file_extension = os.path.splitext(os.path.basename(inputfile))
	listOfFilenameComponents = filename.split("_")
	parentFolder = os.path.abspath(os.path.join(inputfile,'..'))
	listOfFilenameComponents.append(parentFolder)
	fileInfo = ','.join(listOfFilenameComponents)
	outputfile = filename + ".temp"
	wd = os.getcwd()
	osCommand = OSPATH + OSEXEC + " -C " + OSPATH + OSCONF + "/" + configfile + " -I " + inputfile + " -O " + outputfile
	#print(osCommand)
	filename = filename.replace(wd+ '/DAICWOZ/3secOver0/' ,"")
	os.system(osCommand)
	fileHandle = open(outputfile,"r") 
	lineList =fileHandle.readlines()
	fileHandle.close()
	os.system("rm " + outputfile)
	#print(fileInfo)
	data =fileInfo + "," + str([float(i) for i in lineList[-1].split(",")[1:-1]])[1:-1]	
#	print( lineList[1])
	return data



