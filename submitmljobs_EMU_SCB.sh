#!/bin/bash
for f in ./FeatureFiles/EMU/176FeaturesSCB/FeatureSelected*original*.csv; do echo 
echo "Processing $f file.."; 
sbatch ./mljob.sh $f 5 -1 down emu_folds.json
done
for f in ./FeatureFiles/EMU/176FeaturesSCB/FeatureSelected*original*.csv; do echo 
echo "Processing $f file.."; 
sbatch ./mljob.sh $f 5 -1 up emu_folds.json
done
for f in ./FeatureFiles/EMU/176FeaturesSCB/FeatureSelected*original*.csv; do echo 
echo "Processing $f file.."; 
sbatch ./mljob.sh $f 5 -1 None emu_folds.json
done
