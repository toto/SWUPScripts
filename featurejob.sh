#!/bin/bash
#SBATCH --job-name=OSFeatures
#SBATCH --mem=10Gb
#SBATCH -n 8
#SBATCH -t 12:00:00
python selectfeatures.py --filename $1 --dataStart 8 --dataEnd -1 --targetData -1 --cutoff 10 --importancethreshold 1.5 --outputfile $2
