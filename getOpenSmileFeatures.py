# Author: Ermal Toto - 2019
# Co-Authors: (Add your self here when you modify substantially)
import os


def toCsv(content):
    data = False
    header = ""
    newContent = []
    for line in content:
        if not data:
            if "@attribute" in line:
                attri = line.split()
                columnName = attri[attri.index("@attribute") + 1]
                header = header + columnName + ","
            elif "@data" in line:
                data = True
                header = header[:-1]
                header += '\n'
                newContent.append(header)
        else:
            newContent.append(line)
    return newContent


def getFileInfo(dataset, filename, inputfile, configfile, OSPATH):
    if dataset == "DAIC":
        fileInfo = filename.split("_")
        parentFolder = os.path.abspath(os.path.join(inputfile, '..'))
        fileInfo.append(parentFolder)
        fileInfo.append(configfile)
        fileInfo.append(filename)
        fileInfo.append(OSPATH)
        print("Additional Header Elements:" + str(fileInfo))
        print("Num Additional Header Elements:" + str(len(fileInfo)))
        if len(fileInfo) == 8:
            headerInfo = ["ParticipantID", "QuestionName", "QuestionNumber", "ClipNumber", "Dataset", "ConfigFile",
                          "Filename", "OpenSmilePath"]
        elif len(fileInfo) == 7:
            headerInfo = ["ParticipantID", "QuestionName", "QuestionNumber", "Dataset", "ConfigFile", "Filename",
                          "OpenSmilePath"]
    else: #if dataset == "EMU" or dataset == "Moodable" or dataset == "MoodableEMU" or dataset ="EMUOpen":
        fileInfo = filename.split("_")
        fileInfo[0] = fileInfo[0].split(".")[0]
        parentFolder = os.path.abspath(os.path.join(inputfile, '..'))
        fileInfo.append(parentFolder)
        fileInfo.append(configfile)
        fileInfo.append(filename)
        fileInfo.append(OSPATH)
        print("Additional Header Elements:" + str(fileInfo))
        print("Num Additional Header Elements:" + str(len(fileInfo)))
        if len(fileInfo) == 6:
            headerInfo = ["ParticipantID", "ClipNumber", "Dataset", "ConfigFile", "Filename", "OpenSmilePath"]
        if len(fileInfo) == 5:
            headerInfo = ["ParticipantID", "Dataset", "ConfigFile", "Filename", "OpenSmilePath"]
            
    headerInfo = ','.join(headerInfo)
    fileInfo = ','.join(fileInfo)
    return fileInfo, headerInfo, parentFolder


def getFeatures(inputfile, configfile, OSPATH, OSEXEC, OSCONF, isHeader, dataset):
    filename, file_extension = os.path.splitext(os.path.basename(inputfile))
    fileInfo, headerInfo, parentFolder = getFileInfo(dataset, filename, inputfile, configfile, OSPATH)
    outputfile = "./temp/" + parentFolder.replace('/', '') + filename + ".temp"
    osCommand = OSPATH + OSEXEC + " -C " + OSPATH + OSCONF + "/" + configfile + " -I " + inputfile + " -O " + outputfile

    # print(osCommand)
    os.system(osCommand)
    fileHandle = open(outputfile, "r")
    lineList = fileHandle.readlines()
    new = toCsv(lineList)
    header = new[0].split(",")
    del header[0]
    del header[-1]
    header_string = ",".join(header)
    fileHandle.close()
    # os.system("rm " + outputfile)  # print(fileInfo)

    if isHeader:
        col_names = headerInfo + "," + header_string
        data = fileInfo + "," + str([float(i) for i in lineList[-1].split(",")[1:-1]])[1:-1]
        data = col_names + "\n" + data
    else:
        data = fileInfo + "," + str([float(i) for i in lineList[-1].split(",")[1:-1]])[1:-1]

    # int( lineList[1]
    return data
