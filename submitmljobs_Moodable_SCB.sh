#!/bin/bash
for f in ./FeatureFiles/Moodable/176FeaturesSCB/FeatureSelected*original*.csv; do echo 
echo "Processing $f file.."; 
sbatch ./mljob.sh $f 5 -1 down moodable_foldcache.json
done
for f in ./FeatureFiles/Moodable/176FeaturesSCB/FeatureSelected*original*.csv; do echo 
echo "Processing $f file.."; 
sbatch ./mljob.sh $f 5 -1 up moodable_foldcache.json
done
for f in ./FeatureFiles/Moodable/176FeaturesSCB/FeatureSelected*original*.csv; do echo 
echo "Processing $f file.."; 
sbatch ./mljob.sh $f 5 -1 None moodable_foldcache.json
done
