conf_mat = metrics.confusion_matrix(master_y_true, master_y_pred)
f1  = metrics.f1_score(master_y_true, master_y_pred)
accuracy = metrics.accuracy_score(master_y_true, master_y_pred)
roc = metrics.roc_auc_score(master_y_true, master_y_score[:,1])
sensitivity = metrics.recall_score(master_y_true, master_y_pred)
precision = metrics.precision_score(master_y_true, master_y_pred)
TP = conf_mat[0][0]
TN = conf_mat[1][1]
FP = conf_mat[0][1]
FN = conf_mat[1][0]


#if printResultHeader == "True":
print("filename,cutoff,targetData,resampleType,modelType,missingValues,dataStart,dataEnd,targetDataCount,svckernel,numNeighbors,precision,sensitivity,f1,accuracy,roc,TP,TN,FP,FN")
print(filename,",",cutoff,",",targetData,",",resampleType,",",modelType,",",missingValues,",",dataStart,",",dataEnd,",",targetDataCount,",",svckernel,",",numNeighbors,",",precision,",",sensitivity,",",f1,",",accuracy,",",roc,",",TP,",",TN,",",FP,",",FN)
