#!/bin/bash
for f in ./FeatureFiles/EMUOpen/176FeaturesSCB/FeatureSelected*.csv; do echo 
echo "Processing $f file.."; 
sbatch ./mljob.sh $f 5 -1 down emu_open_folds.json
done
for f in ./FeatureFiles/EMUOpen/176FeaturesSCB/FeatureSelected*.csv; do echo 
echo "Processing $f file.."; 
sbatch ./mljob.sh $f 5 -1 up emu_open_folds.json
done
for f in ./FeatureFiles/EMUOpen/176FeaturesSCB/FeatureSelected*.csv; do echo 
echo "Processing $f file.."; 
sbatch ./mljob.sh $f 5 -1 None emu_open_folds.json
done
