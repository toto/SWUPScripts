#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import argparse
import os
import numpy as np
import pandas as pd # Pandas is used to import the CSV file
import metricTools as mt
import warnings
import json
warnings.filterwarnings("ignore")

folder = "./Results/DAIC/"
cutoff = 10
question = False
weight = 1.1
aggregations = ["mean","median","max","voting"]
isfeaturesel = False

parser = argparse.ArgumentParser()
parser.add_argument("--cutoff", type=int, help="Target Variable cutoff for binary classification")
parser.add_argument("--folder", help="Input Folder")
parser.add_argument("--weights",  help='Probability Weight Range {"Start": 0.5, "End": 1.5,"Increment":0.1}')
parser.add_argument("--isfeaturesel", type=bool, help="Results Are for feature selection purposes. Default: False")

#parser.add_argument("--aggregation",  help='Aggregation type. Default: mean')

args = parser.parse_args()
if args.__dict__["cutoff"]  is not None:
    cutoff = args.__dict__["cutoff"]
if args.__dict__["isfeaturesel"]  is not None:
    isfeaturesel = args.__dict__["isfeaturesel"]    
if args.__dict__["folder"]  is not None:
    folder = args.__dict__["folder"]
if args.__dict__["weights"]  is not None:
    weights = args.__dict__["weights"]    
weights = json.loads(weights)    
    
# args.__dict__["aggregation"]  is not None:
#    aggregation = args.__dict__["aggregation"] 
    
firstFile = True
for filename in os.listdir(folder):
    if filename.endswith(".csv"): 
        data = pd.read_csv(os.path.join(folder, filename),index_col=0) 
        data.dropna(subset=['binaryTruth'], inplace=True)
        data.dropna(subset=['binaryPrediction'], inplace=True)
        
        data = data[data['binaryTruth'].isin([1, 0])] 
        data = data[data['binaryPrediction'].isin([1, 0])] 

        fileinfo = filename.split("_")
        if fileinfo[5] == "Labeled":
            fileinfo.insert(5, "NotFeatureSelected")
            
        if fileinfo[0] == "DAIC":
            data_mean = data.groupby(["ParticipantID","QuestionNumber"]).mean()
            data_median = data.groupby(["ParticipantID","QuestionNumber"]).mean()
            data_sum = data.groupby(["ParticipantID","QuestionNumber"]).sum()     
            data_max = data.groupby(["ParticipantID","QuestionNumber"]).max() 
            data_count = data.groupby(["ParticipantID","QuestionNumber"]).count() 

            grouping = "participantandquestion"
            question = True
        else:
            data_mean = data.groupby(["ParticipantID"]).mean()
            data_median = data.groupby(["ParticipantID"]).median()
            data_sum = data.groupby(["ParticipantID"]).sum()      
            data_max = data.groupby(["ParticipantID"]).max()            
            data_count = data.groupby(["ParticipantID"]).count()            

            grouping = "participant"
            question = False
        for aggregation in aggregations:
            if aggregation == "sum":
                data = data_sum
                data["probabilityOfZero"] = data_mean["probabilityOfZero"]
                data["probabilityOfOne"] = data_mean["probabilityOfOne"]
            elif aggregation == "mean":
                data = data_mean
            elif aggregation == "median":
                data = data_median
            elif aggregation == "max":
                data = data_max 
            elif aggregation == "voting":
                data = data_sum
                data["probabilityOfZero"] = data_mean["probabilityOfZero"]
                data["probabilityOfOne"] = data_mean["probabilityOfOne"]
                data["binaryTruth"] = data_mean["binaryTruth"]

                
            for weight in np.arange(weights["Start"],weights["End"],weights["Increment"]):
                if aggregation == "voting":
                    data["binaryPrediction"] = np.where(data["binaryPrediction"]*weight >= data_count["binaryPrediction"]/2, 1, 0)
                else:
                    data["binaryPrediction"] = np.where(data["probabilityOfOne"]*weight >= data["probabilityOfZero"], 1, 0)
                
                #print(str(fileinfo[0])+"-->" + str(len(data_mean_sum)))
                mt.dataToCSVHelper(data,fileinfo,grouping,cutoff,weight,aggregation,isfeaturesel,firstFile)
                firstFile = False
