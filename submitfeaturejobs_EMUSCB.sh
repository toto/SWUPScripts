#!/bin/bash
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_2clipsOver0.25OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_2clipsOver0.25OpenSmile.csv
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_2clipsOver0.5OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_2clipsOver0.5OpenSmile.csv
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_2clipsOver0.75OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_2clipsOver0.75OpenSmile.csv
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_2clipsOver0OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_2clipsOver0OpenSmile.csv
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_3clipsOver0.25OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_3clipsOver0.25OpenSmile.csv
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_3clipsOver0.5OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_3clipsOver0.5OpenSmile.csv
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_3clipsOver0.75OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_3clipsOver0.75OpenSmile.csv
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_3clipsOver0OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_3clipsOver0OpenSmile.csv
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_4clipsOver0.25OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_4clipsOver0.25OpenSmile.csv
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_4clipsOver0.5OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_4clipsOver0.5OpenSmile.csv
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_4clipsOver0.75OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_4clipsOver0.75OpenSmile.csv
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_4clipsOver0OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_4clipsOver0OpenSmile.csv
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_5clipsOver0.25OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_5clipsOver0.25OpenSmile.csv
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_5clipsOver0.5OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_5clipsOver0.5OpenSmile.csv
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_5clipsOver0.75OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_5clipsOver0.75OpenSmile.csv
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_5clipsOver0OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_5clipsOver0OpenSmile.csv
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_6clipsOver0.25OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_6clipsOver0.25OpenSmile.csv
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_6clipsOver0.5OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_6clipsOver0.5OpenSmile.csv
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_6clipsOver0.75OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_6clipsOver0.75OpenSmile.csv
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_6clipsOver0OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_6clipsOver0OpenSmile.csv
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_7clipsOver0.25OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_7clipsOver0.25OpenSmile.csv
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_7clipsOver0.5OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_7clipsOver0.5OpenSmile.csv
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_7clipsOver0.75OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_7clipsOver0.75OpenSmile.csv
sbatch featurejob.sh /work/shared/mhealthmqp/EMU/osfeatures/Labeled_NoMissing_7clipsOver0OpenSmile.csv /work/shared/mhealthmqp/EMU/osfeatures/FeatureSelected_Labeled_NoMissing_7clipsOver0OpenSmile.csv

